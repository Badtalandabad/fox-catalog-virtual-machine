class nodejspack {
  class { 'nodejs':
    repo_url_suffix => '16.x',
  }

  package { '@vue/cli':
    ensure   => 'present',
    provider => 'npm',
  }
}
