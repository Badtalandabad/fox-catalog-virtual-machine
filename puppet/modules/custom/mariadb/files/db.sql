/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for foxcatalog
CREATE DATABASE IF NOT EXISTS `foxcatalog` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;
USE `foxcatalog`;

-- Dumping structure for table foxcatalog.genera
CREATE TABLE IF NOT EXISTS `genera` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_on` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_on` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `article` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table foxcatalog.genera: ~6 rows (approximately)
/*!40000 ALTER TABLE `genera` DISABLE KEYS */;
INSERT INTO `genera` (`id`, `created_on`, `updated_on`, `slug`, `icon`, `name`, `article`, `image`) VALUES
	(1, '2021-10-10 20:35:46', '2021-10-23 20:35:28', NULL, NULL, 'Canis', 'Canis is a genus of the Caninae which includes multiple extant species, such as wolves, dogs, coyotes, and golden jackals. Species of this genus are distinguished by their moderate to large size, their massive, well-developed skulls and dentition, long legs, and comparatively short ears and tails.', 'https://upload.wikimedia.org/wikipedia/commons/3/31/Canis_portraits_%28excluding_Lupulella%29.jpg'),
	(2, '2021-10-10 20:42:18', '2021-10-23 20:38:44', NULL, NULL, 'Cerdocyon', 'Cerdocyonina is a tribe which appeared around 6.0 million years ago (Mya) in North America as Ferrucyon avius becoming extinct by around 1.4–1.3 Mya. living about 4.7 million years. This genus has persisted in South America from an undetermined time, possibly around 3.1 Mya, and continues to the present in the same or a similar form to the crab-eating fox. As one of the species of the tribe Canini, it is related to the genus Canis. The crab-eating fox\'s nearest living relative, as theorized at present, is the short-eared dog. This relationship, however, has yet to be supported by mitochondrial investigations. Two subgenera (Atelocynus and Speothos) were long ago included in Cerdocyon.', 'https://upload.wikimedia.org/wikipedia/commons/thumb/4/46/Crab-eating_Fox.JPG/1280px-Crab-eating_Fox.JPG'),
	(3, '2021-10-10 20:49:53', '2021-10-23 20:39:02', NULL, NULL, 'Lycalopex (South American fox)', 'The South American foxes (Lycalopex), commonly called raposa in Portuguese, or zorro in Spanish, are a genus from South America of the subfamily Caninae. Despite their name, they are not true foxes, but are a unique canid genus more closely related to wolves and jackals than to true foxes; some of them resemble foxes due to convergent evolution. The South American gray fox, Lycalopex griseus, is the most common species, and is known for its large ears and a highly marketable, russet-fringed pelt. The second-oldest known fossils belonging to the genus were discovered in Chile, and date from 2.0 to 2.5 million years ago, in the mid- to late Pliocene. The Vorohué Formation of Argentina has provided older fossils, dating to the Uquian to Ensenadan (Late Pliocene).', 'https://upload.wikimedia.org/wikipedia/commons/5/5c/Genus_pseudalopex.jpg'),
	(4, '2021-10-10 20:57:44', '2021-10-23 20:39:56', NULL, NULL, 'Otocyon', 'The bat-eared fox is the only living species of the genus Otocyon. Its scientific name, given by Anselme Gaëtan Desmarest, was initially Canis megalotis (due to its close resemblance to jackals), and later changed by Salomon Müller which placed it in its own genus, Otocyon; its huge ears and different dental formula warrant inclusion in a genus distinct from both Canis and true foxes (Vulpes). The generic name Otocyon is derived from the Greek words otus for ear and cyon for dog, while the specific name megalotis comes from the Greek words mega for large and otus for ear. Due to its different dentition, the bat-eared fox were previously placed in a distinct subfamily of canids, Otocyoninae, as no relationship to any living species of canid could be established. However, according to more recent examinations, this species is regarded as having affinities with the vulpine line, and Otocyon was placed with high confidence as sister to the clade containing both the raccoon dog (Nyctereutes) and true foxes (Vulpes), occupying a basal position within Canidae.', 'https://upload.wikimedia.org/wikipedia/commons/thumb/8/84/Otocyon_megalotis_-_Etosha_2014.jpg/1920px-Otocyon_megalotis_-_Etosha_2014.jpg'),
	(5, '2021-10-10 21:00:52', '2021-10-23 20:40:12', NULL, NULL, 'Urocyon', 'Urocyon (Greek) is a genus of Canidae which includes the gray fox (Urocyon cinereoargenteus) and the island fox (Urocyon littoralis). These two fox species are found in the Western Hemisphere. Whole genome sequencing indicates that Urocyon is the most basal genus of the living canids. Fossils of what is believed to be the ancestor of the gray fox, Urocyon progressus, have been found in Kansas and date to the Upper Pliocene, with some undescribed specimens dating even older.', 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/2e/Urocyon_cinereoargenteus.jpg/1920px-Urocyon_cinereoargenteus.jpg'),
	(6, '2021-10-10 21:11:25', '2021-10-23 20:40:26', NULL, NULL, 'Vulpes', 'Vulpes is a genus of the sub-family Caninae. The members of this genus are colloquially referred to as true foxes, meaning they form a proper clade. The word "fox" occurs in the common names of species. True foxes are distinguished from members of the genus Canis, such as domesticated dogs, wolves, jackals and coyotes, by their smaller size (5–11 kg), longer, bushier tail, and flatter skull. They have black, triangular markings between their eyes and nose, and the tip of their tail is often a different color from the rest of their pelt. The typical lifespan for this genus is between two and four years, but can reach up to a decade.', 'https://upload.wikimedia.org/wikipedia/commons/thumb/c/cf/Genus_vulpes.jpg/800px-Genus_vulpes.jpg');
/*!40000 ALTER TABLE `genera` ENABLE KEYS */;

-- Dumping structure for table foxcatalog.species
CREATE TABLE IF NOT EXISTS `species` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_on` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_on` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `genus_id` int(10) unsigned NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `article` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `name` (`name`) USING BTREE,
  UNIQUE KEY `slug` (`slug`) USING BTREE,
  KEY `FK_species_genera` (`genus_id`),
  CONSTRAINT `FK_species_genera` FOREIGN KEY (`genus_id`) REFERENCES `genera` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- Dumping data for table foxcatalog.species: ~0 rows (approximately)
/*!40000 ALTER TABLE `species` DISABLE KEYS */;
/*!40000 ALTER TABLE `species` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
