class python {
  $packages = [
  'python3-pip', 
  'python3-dev', 
  'build-essential', 
  'libffi-dev', 
  'python3-setuptools', 
  'python3-venv'
  ]

  package { $packages:
    ensure => 'present',
  }

  $envname = 'flaskenv'
  file { "/etc/systemd/system/${project}.service":
    mode    => '0644',
    owner   => 'root',
    group   => 'root',
    content => template('python/gunicornservice.erb'),
  }~>
  exec { 'myservice-systemd-reload':
    command => 'systemctl daemon-reload',
    path    => ['/usr/bin', '/bin', '/usr/sbin'],
    refreshonly => true,
  }~>
  service { $project:
    enable => true,
  }
}